const urlParams = new URLSearchParams(window.location.search);
const fast = urlParams.get('fast')?urlParams.get('fast')=="true":false;
const table = document.querySelector("#prenoms tbody");
const colonne = 5;

let start = new Date();

/* Randomize array in-place using Durstenfeld shuffle algorithm */
function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

function load(next){
  fetch('prenoms.txt')
    .then(function(response) {
      return response.text();
    })
    .then(function(content) {
      let mots = content.split("\n");
      next(mots);
    });
}

function print(mots){
    const ligne = Math.floor(mots.length / colonne);

    let indice = 0;

    for (let i = 0 ; i < ligne; i++){
      let tr = document.createElement('tr');
      table.appendChild(tr);
      for (let j = 0; j < colonne; j++){
        let td = document.createElement('td');

        if (indice < mots.length){
          td.innerHTML = mots[indice];
        }
        td.addEventListener('click', function(){
          const now = new Date();
          alert("Vous avez trouvé(e) votre prénom ('" + this.innerHTML + "') en " + ((now - start) / 1000) + " secondes.");
          start = new Date();
        });
        tr.appendChild(td);

        indice++;
      }
    }
}

load(function(mots){
  if (!fast){
    shuffle(mots);
  }
  print(mots);
});
